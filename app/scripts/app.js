'use strict';

/**
 * @ngdoc overview
 * @name capsotestApp
 * @description
 * # capsotestApp
 *
 * Main module of the application.
 */

var app;

app = angular.module('capsotestApp', [
  'ui.router',
  'ngAnimate',
  'ngResource',
  'ngSanitize',
  'ngTouch',
  'underscore'
]);

app.config(function ($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('main', {
      url: '/',
      templateUrl: 'views/main.html',
      controller: 'MainCtrl'
    })
    .state('list', {
      url: '/list',
      templateUrl: 'views/companies.list.html',
      controller: 'ListCtrl'
    })
    .state('details', {
      url: '/details/{companyId}',
      templateUrl: 'views/details.html',
      controller: 'DetailsCtrl'
    });
});

app.controller('ListCtrl', ['$scope', '$http', 'companies', function ($scope, $http, companies) {
  companies.list(function (companies) {
    $scope.companies = companies;
  });
}]);


app.controller('DetailsCtrl', ['$scope', '$http', '$cacheFactory', '$stateParams', 'companies', 'companyDetails',
  function ($scope, $http, $cacheFactory, $stateParams, companies, companyDetails) {

    var companyId = $stateParams.companyId && (typeof $stateParams.companyId === 'string') ? parseInt($stateParams.companyId) : null;
    if (!companyId) {
      return 'Company id is not defined';
    }

    $scope.items = [];
    $scope.onlyMax = false;

    if ($scope.timeDiff === '1day') {
      app.timeDiff = 1;
    } else if ($scope.timeDiff === '1week') {
      app.timeDiff = 7;
    } else if ($scope.timeDiff === '1month') {
      app.timeDiff = 30;
    }

    companies.find(companyId, function (companyData) {
      $scope.companyData = companyData;
      companyDetails.find(companyId, function (companyDetails) {
        $scope.companyDetails = companyDetails;
        var prices = [];
        angular.forEach(companyDetails, function (cd) {
          angular.forEach(cd.prices, function (p, k) {
            if (k > 100) {
              return;
            }
            prices.push({
              cuponRate: cd.couponRate,
              cusip: cd.cusip,
              timestamp: p.timestamp,
              date: moment(p.timestamp).format('YYYY-MM-DD'),
              price: p.price
            });
          });
          $scope.items.push({name: cd.couponRate});
        });
        $scope.prices = prices;
      });

      $scope.showMax = function () {
        $scope.onlyMax = true;
        app.onlyMax = true;
      };

      $scope.showAll = function () {
        $scope.onlyMax = false;
        app.onlyMax = false;
      };

      $scope.setTimeDiff = function(time){
        app.timeDiff = time;
      }
    });
  }]);

app.factory('companyDetails', function ($http) {
  function getData(callback) {
    $http({
      method: 'GET',
      url: '/details',
      cache: true
    }).success(function (data) {
      callback(data);
    });
  }

  return {
    list: getData,
    find: function (companyId, callback) {
      getData(function (data) {
        var details = [];
        angular.forEach(data, function (d) {
          if (d.issuerId === companyId) {
            details.push(d);
          }
        });
        callback(details);
      });
    }
  };
});

app.factory('companies', function ($http) {
  function getData(callback) {
    $http({
      method: 'GET',
      url: '/companies-list',
      cache: true
    }).success(function (data) {
      callback(data.results);
    });
  }

  return {
    list: getData,
    find: function (companyId, callback) {
      getData(function (data) {
        var company = data.filter(function (entry) {
          return entry.companyId === companyId;
        })[0];
        callback(company);
      });
    }
  };
});

app.factory('companyData', function () {
  return {
    data: [],
    onlyMax: false
  };
});

app.filter('hasPrice', function () {
  return function (items, selectedItem) {
    if (!selectedItem) {
      return items;
    }

    var dayDiff = app.timeDiff || 1;
    //console.log(app.timeDiff);
    var today = moment();
    var filtered = [];
    angular.forEach(items, function (item) {
      if (filtered.length === 10 && app.onlyMax) {
        if (today.diff(moment(item.timestamp), 'days') === dayDiff) {
          if (item.cuponRate === selectedItem.name) {
            filtered.push(item);
          }
        }
      } else {
        if (item.cuponRate === selectedItem.name) {
          filtered.push(item);
        }
      }
    });

    return filtered;
  };
});

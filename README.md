This repository contains assessment for CapSo first stage interview.
To install application please 

* Use bower and npm to install dependencies

* Run using grunt serve

* Go to localhost:9000 and further instructions will be provided there.


Application has 2 roots

* list - list of companies
![Screen Shot 2015-10-04 at 16.48.03.png](https://bitbucket.org/repo/KKGxdX/images/1841766934-Screen%20Shot%202015-10-04%20at%2016.48.03.png)

* details/{companyId} - displays details of each company along with prices history
![Screen Shot 2015-10-04 at 16.48.12.png](https://bitbucket.org/repo/KKGxdX/images/4156603119-Screen%20Shot%202015-10-04%20at%2016.48.12.png)

For proxing requests from amazon AWS I used Charles proxy (http://www.charlesproxy.com/) with following configuration

![Screen Shot 2015-10-04 at 15.56.42.png](https://bitbucket.org/repo/KKGxdX/images/1615003991-Screen%20Shot%202015-10-04%20at%2015.56.42.png)

But this task can be also easily achieved using grunt-connect-proxy (https://github.com/drewzboto/grunt-connect-proxy).
In application I used normal ng-repeat for rendering, but for rendering huge sets of data would be better to use react.js components since they are much more faster that normal angular components.